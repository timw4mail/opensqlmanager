<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

namespace OpenSQLManager;

/**
 * Class for manipulating datbase connections, and miscellaneous settings
 *
 * @package OpenSQLManager
 * @subpackage Common
 */
class Settings {

	/**
	 * Settings object represented by the currently loaded JSON file
	 */
	private $current;

	/**
	 * Singleton instance
	 */
	private static $instance;

	/**
	 * Static method to retreive current instance
	 * of the singleton
	 *
	 * @return Settings
	 */
	public static function &get_instance()
	{
		if( ! isset(self::$instance))
		{
			$name = __CLASS__;
			self::$instance = new $name();
		}

		return self::$instance;
	}

	/**
	 * Load the settings file - private so it can't be loaded
	 * directly - the settings should be safe!
	 */
	private function __construct()
	{
		// For testing and use outside of OpenSQLManager,
		// define a different settings directory
		if ( ! defined('OSM_SETTINGS_DIR'))
		{
			define('OSM_SETTINGS_DIR', '.');
		}

		$path = OSM_SETTINGS_DIR.'/settings.json';

		if( ! is_file($path))
		{
			//Create the file!
			file_put_contents($path, '{}');
			$this->current = new \stdClass();
		}
		else
		{
			$this->current = json_decode(file_get_contents($path));

			if (empty($this->current))
			{
				$this->current = new \stdClass();
				file_put_contents($path, '{}');
			}

		}

		// Add the DB object under the settings if it doesn't already exist
		if( ! isset($this->current->dbs))
		{
			$this->current->dbs = new \stdClass();
		}

	}

	// --------------------------------------------------------------------------

	/**
	 * Output the settings on destruct
	 */
	public function __destruct()
	{
		$file_string = (defined('JSON_PRETTY_PRINT'))
			? json_encode($this->current, JSON_PRETTY_PRINT)
			: json_encode($this->current);

		file_put_contents(OSM_SETTINGS_DIR . '/settings.json', $file_string);
	}

	// --------------------------------------------------------------------------

	/**
	 * Magic method to simplify isset checking for config options
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function __get($key)
	{
		return (isset($this->current->{$key}) && $key != "dbs")
			? $this->current->{$key}
			: NULL;
	}

	// --------------------------------------------------------------------------

	/**
	 * Magic method to simplify setting config options
	 *
	 * @param string $key
	 * @param mixed
	 */
	public function __set($key, $val)
	{
		//Don't allow direct db config changes
		if($key == "dbs")
		{
			return FALSE;
		}

		return $this->current->{$key} = $val;
	}

	// --------------------------------------------------------------------------

	/**
	 * Add a database connection
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function add_db($name, $params)
	{
		// Return on bad data
		if (empty($name) || empty($params))
		{
			return FALSE;
		}

		if ( ! isset($this->current->dbs->{$name}))
		{
			$params->name = $name;

			$this->current->dbs->{$name} = array();
			$this->current->dbs->{$name} = $params;
		}
		else
		{
			return FALSE;
		}

		// Save the json
		$this->__destruct();
	}

	// --------------------------------------------------------------------------

	/**
	 * Edit a database connection
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function edit_db($name, $params)
	{
		// Return on bad data
		if (empty($name) || empty($params))
		{
			return FALSE;
		}

		if (is_object($params))
		{
			$params = (array) $params;
		}

		if (isset($this->current->dbs->{$name}) && ($name === $params->name))
		{
			$this->current->dbs->{$name} = $params;
		}
		elseif ($name !== $params->name)
		{
			unset($this->current->dbs->{$name});

			if ( ! isset($this->current->dbs->{$params->name}))
			{
				$this->current->dbs->{$params->name} = $params;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

		// Save the json
		$this->__destruct();

		return TRUE;
	}

	// --------------------------------------------------------------------------

	/**
	 * Remove a database connection
	 *
	 * @param  string $name
	 */
	public function remove_db($name)
	{
		if( ! isset($this->current->dbs->{$name}))
		{
			return FALSE;
		}

		// Remove the db name from the object
		unset($this->current->dbs->{$name});

		// Save the json
		$this->__destruct();
	}

	// --------------------------------------------------------------------------

	/**
	 * Retreive all db connections
	 *
	 * @return  array
	 */
	public function get_dbs()
	{
		return $this->current->dbs;
	}

	// --------------------------------------------------------------------------

	/**
	 * Retreive a specific database connection
	 *
	 * @param string $name
	 * @return object
	 */
	public function get_db($name)
	{
		return (isset($this->current->dbs->{$name}))
			? $this->current->dbs->{$name}
			: FALSE;
	}

}
// End of settings.php