<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

/**
 * Global Functions
 *
 * @package OpenSQLManager
 * @subpackage Common
 */

namespace OpenSQLManager;

/**
 * Convert an array to an object
 *
 * @param array $array
 * @return object
 */
function array_to_object($array)
{
	if (is_object($array))
	{
		return $array;
	}

	$obj = new \StdClass();

	foreach($array as $k => $v)
	{
		$obj->$k = $v;
	}

	return $obj;
}

// --------------------------------------------------------------------------

/**
 * Create info dialog to return an informational message
 *
 * @param  string $message
 * @return void
 */
function alert($message)
{
	return \wxMessageBox($message, 'Info', wxOK|wxICON_INFORMATION);
}

// --------------------------------------------------------------------------

/**
 * Create info dialog to retun an informational message
 *
 * @param string $message
 * @return void
 */
function error($message)
{
	return \wxMessageBox($message, 'Error', wxOK|wxICON_ERROR);
}

// --------------------------------------------------------------------------

/**
 * Creates a binary confirmation dialog
 *
 * @param string $message
 * @return bool
 */
function confirm($message)
{
	$answer = \wxMessageBox($message, 'Prompt', wxYES_NO);
	return ($answer === wxYES) ? TRUE : FALSE;
}

// End of functions.php