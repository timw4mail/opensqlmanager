<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

namespace OpenSQLManager;

/**
 * Main Window Class
 *
 * Creates and displays the main interface window
 *
 * @package OpenSQLManager
 * @subpackage Windows
 */
class Main extends \wxFrame {
	
	const MAC_EXIT_ITEM = 100;
	const MAC_ABOUT_ITEM = 101;

	/**
	 * Reference to settings instance
	 *
	 * @var Settings
	 */
	private $settings;

	/**
	 * Reference to connection sidebar instance
	 *
	 * @var Connection_Sidebar
	 */
	private $connection_sidebar;

	/**
	 * Reference to split window
	 *
	 * @var wxSplitterWindow
	 */
	protected $split;
	
	/**
	 * Reference to tab control in split Window
	 *
	 * @var wxAUINotebook
	 */
	protected $tabs;

	/**
	 * Create and display the main window on startup
	 */
	public function __construct()
	{
		parent::__construct(NULL, NULL, OSM_PROGRAM_NAME, \wxDefaultPosition, new \wxSize(800, 480));
		$this->_create_menu();

		$sbar = $this->CreateStatusBar(2);
		$sbar->SetStatusText("OpenSQLManager");

		$this->settings =& Settings::get_instance();

		// Layout the interface
		$this->_main_layout();
		$this->CenterOnScreen(wxBOTH);
	}

	// --------------------------------------------------------------------------

	/**
	 * Some cleanup for when the main window is closed
	 *
	 * @return void
	 */
	public function __destruct()
	{
		$this->Destroy();
	}

	// --------------------------------------------------------------------------

	/**
	 * Exits the wx loop
	 *
	 * @return void
	 */
	public function quit()
	{
		$this->__destruct();
	}

	// --------------------------------------------------------------------------

	/**
	 * Display About menu with version information
	 *
	 * @return void
	 */
	public function about()
	{
		$dlg = new \wxAboutDialogInfo();

		$dlg->SetName(OSM_PROGRAM_NAME);
		$dlg->SetVersion(OSM_VERSION);

		$dlg->SetCopyright("Copyright (c) ".date('Y')." Timothy J. Warren");

		$dlg->SetWebSite('https://github.com/aviat4ion/OpenSQLManager','Fork on Github');

		$dlg->SetLicense(file_get_contents(OSM_RESOURCE_DIR . "/LICENSE"));

		$dlg->SetDevelopers(array(
			'Timothy J. Warren',
		));

		\wxAboutBox($dlg);
	}

	// --------------------------------------------------------------------------
	
	/**
	 * Loads data tabs for the selected database
	 *
	 * @param string $dbname
	 */
	public function load_tabs($dbname)
	{
		
	}
		
	// --------------------------------------------------------------------------

	/**
	 * Layout the main interface
	 * Create menus, hboxes, vboxs and other widgets
	 *
	 * @return void
	 */
	private function _main_layout()
	{
		// Set up the main menu
		$this->_create_menu();

		// Create a split window
		$win = new \wxSplitterWindow($this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_THIN_SASH);
		$win->setSplitMode(wxSPLIT_HORIZONTAL);

		// Add the connection sidebar
		$this->connection_sidebar =& Connection_Sidebar::get_instance($win);
		$win2 = new Db_tabs($win);
		$win2->add_tab('');

		// Add the widgets to the split window
		$win->SplitVertically($this->connection_sidebar, $win2);
		$win->SetSashPosition(200, TRUE);
		
		// Save a reference for later use
		$this->split =& $win;
		$this->tabs =& $win2;
	}

	// --------------------------------------------------------------------------

	/**
	 * Create the menu for the program
	 *
	 * @return void
	 */
	private function _create_menu()
	{	
		// Menu Bar
		$menu_bar = new \wxMenuBar();

		// Menu Bar Top Items
		$top_file_menu = new \wxMenu();
		$top_edit_menu = new \wxMenu();
		//$top_export_menu = new \wxMenu();
		$top_help_menu = new \wxMenu();

		// File Menu
		{
			// Set up the quit item
			$top_file_menu->Append(wxID_EXIT, "&Quit\tCtrl+Q", "Exit the program");
			$this->Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, array($this, "quit"));
			
			

			// Add the top level menu to the menubar
			$menu_bar->Append($top_file_menu, "&File");
		}
		
		// Edit Menu
		{
			$top_edit_menu->Append(wxID_CUT, "Cut\tCtrl+X", "Cut");
			$top_edit_menu->Append(wxID_COPY, "Copy\tCtrl+C", "Copy");
			$top_edit_menu->Append(wxID_PASTE, "Paste\tCtrl+V", "Paste");
			
			$menu_bar->Append($top_edit_menu, "&Edit");
		}
		
		// Export Menu
		{	
			// Add the top level menu to the menubar
			//$menu_bar->Append($top_export_menu, "E&xport");
		}		

		// Help Menu
		{
			// Set up the about item
			$top_help_menu->Append(wxID_ABOUT, "&About", "About this program");
			$this->Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, array($this, "about"));
			
			// Add the top level menu to the menubar
			$menu_bar->Append($top_help_menu, "&Help");
			
		}
		
		// For OS X, duplicate the Quit and About menu items
		if (PLATFORM == wxOS_MAC_OSX_DARWIN)
		{
			// Quit
			$top_file_menu->Append(self::MAC_EXIT_ITEM, "&Quit\tCtrl+Q", "Exit the program");
			$this->Connect(self::MAC_EXIT_ITEM, wxEVT_COMMAND_MENU_SELECTED, array($this, "quit"));
			
			// About
			$top_help_menu->Append(self::MAC_ABOUT_ITEM, "&About", "About this program");
			$this->Connect(self::MAC_ABOUT_ITEM, wxEVT_COMMAND_MENU_SELECTED, array($this, "about"));
		}

		$this->SetMenuBar($menu_bar);
	}
}

// End of main.php