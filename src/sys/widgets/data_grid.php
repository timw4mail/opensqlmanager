<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

namespace OpenSQLManager;

/**
 * Class to simplify dealing with GtkTreeView
 *
 * @package OpenSQLManager
 * @subpackage Widgets
 */
class Data_Grid extends \wxGrid {

	/**
	 * Create the object
	 *
	 * @param object
	 */
	public function __construct($parent = NULL)
	{
		if ( ! is_null($parent))
		{
			parent::__construct($parent, wxID_ANY);
			$this->CreateGrid(30,8);
			//$this->HideColLabels();
			//$this->HideRowLabels();
		}
		else
		{
			parent::__construct();
		}
	}
}

// End of data_grid.php