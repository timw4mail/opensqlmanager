<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

namespace OpenSQLManager;

/**
 * Tabbed Container for database properties
 *
 * @package OpenSQLManager
 * @subpackage Widgets
 */
class DB_tabs extends \wxAUINotebook {

	/**
	 * Current Tab Widget object
	 * 
	 * @var DB_Tabs
	 */
	private static $instance;
	
	/**
	 * Db Data cache
	 * 
	 * @var array
	 */
	private $data;
	
	/**
	 * Reference to parent
	 *
	 * @var wxWindow
	 */
	private $parent;

	/**
	 * Return the db tabs object if it exists, or create and return
	 *
	 * @return DB_tabs
	 */
	public static function &get_instance($parent)
	{
		if (empty(self::$instance))
		{
			self::$instance = new DB_tabs($parent);
		}

		return self::$instance;
	}

	// --------------------------------------------------------------------------

	/**
	 * Create the object
	 */
	public function __construct($parent)
	{
		parent::__construct($parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_CLOSE_ON_ALL_TABS | wxAUI_NB_WINDOWLIST_BUTTON);
		$this->parent = $parent;
		$this->data = new \StdClass();
	}

	// --------------------------------------------------------------------------

	/**
	 * Add a new tab with the provided label
	 *
	 * @param string $label
	 * @param GObject $widget
	 * @return void
	 */
	public function add_tab($label, $widget = NULL)
	{
		if (is_null($widget))
		{
			$widget = new Data_Grid($this);
			
		}
		
		$this->AddPage($widget, $label);
	}
}
// End of db_tabs.php