<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

namespace OpenSQLManager;

/**
 * Widget managing saved database connections
 *
 * @package OpenSQLManager
 * @subpackage Widgets
 */
class Connection_Sidebar extends \wxPanel {

	const MENU_CONNECT =  1;
	const MENU_DISCONNECT = 2;
	const MENU_EDIT_CONNECT = 3;
	const MENU_DELETE_CONNECT = 4;
	const BTN_ADD = 5;

	/**
	 * Reference to Settings instance
	 *
	 * @var Settings
	 */
	protected $settings;

	/**
	 * Reference to popup menu
	 *
	 * @var wxMenu
	 */
	protected $menu;

	/**
	 * Singleton instance
	 *
	 * @var Connection_Sidebar
	 */
	private static $instance;

	/**
	 * Name of current db connection
	 *
	 * @var string
	 */
	private $conn_name;

	/**
	 * Reference to the list control that holds the connections
	 *
	 * @var wxListCtrl
	 */
	public $list;
	
	/**
	 * Reference to the image list control that holds connection images
	 *
	 * @var wxImageList
	 */
	private $img_list;
	
	/**
	 * Reference to the parent of the current object
	 *
	 * @var object
	 */
	public $parent;

	/**
	 * Return the current instance of the class
	 *
	 * @param wxWindow
	 * @return Connection_Sidebar
	 */
	public static function &get_instance($parent)
	{
		if( ! isset(self::$instance))
		{
			$name = __CLASS__;
			self::$instance = new $name($parent);
		}

		return self::$instance;
	}

	// --------------------------------------------------------------------------

	/**
	 * Constructor method
	 *
	 * @param wxWindow
	 */
	public function __construct($parent)
	{
		$this->parent =& $parent;
		
		// Create the frame
		parent::__construct($parent, 1);

		// Layout the connection list
		$this->_layout();
	}

	// --------------------------------------------------------------------------

	/**
	 * Right-click event to create context menu
	 *
	 * @param wxEvent
	 * @return void
	 */
	public function menu($event)
	{
		if ($this->list->GetSelectedItemCount() > 0)
		{
			// Create the menu items
			$menu = new \wxMenu();
			$menu->Append(self::MENU_EDIT_CONNECT, "Edit Connection", "Edit Connection Settings for the selected Database");
			$menu->Append(self::MENU_DELETE_CONNECT, "Delete Connection", "Remove the selected connection");

			// Wire up the event handler
			$menu->Connect(wxEVT_COMMAND_MENU_SELECTED, array($this, 'menu_event'));

			// Tell the object to show the menu
			$this->list->PopupMenu($menu);
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Handler for context menu options
	 *
	 * @param wxEvent
	 * @return void
	 */
	public function menu_event($event)
	{
		$id = $event->GetId();
		
		switch($id)
		{
			case self::MENU_EDIT_CONNECT:
				alert("Edit");
			break;
			
			case self::MENU_DELETE_CONNECT:
				$res = confirm("Do you want to delete this item?");
				if ($res)
				{
					print_r($event);
				}
			break;
			
			default:
			break;
		}
	}

	// --------------------------------------------------------------------------

	/**
	 * Handles an event for adding a connection
	 *
	 * @param wxEvent
	 * @return void
	 */
	public function add_conn($event)
	{
		$win = new Connection_Manager($this);
		$win->show();
	}
	
	// --------------------------------------------------------------------------
	
	/**
	 * Connect to the database for data display/manipulation
	 *
	 * @param wxEvent
	 * @return void
	 */
	public function open_connection($event)
	{
		$id = $event->GetId();
		alert('Double click:' . $id);		
	}
	
	// --------------------------------------------------------------------------
	
	/**
	 * Add the existing items to the connection sidebar
	 *
	 * @return void
	 */
	public function _layout()
	{
		$this->list = new \wxListCtrl($this->parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_SMALL_ICON | wxLC_SINGLE_SEL | wxLC_ALIGN_TOP);
		$this->img_list = new \wxImageList(32, 32);
		$this->settings =& Settings::get_instance();
		$this->list->Connect(wxEVT_COMMAND_LIST_ITEM_RIGHT_CLICK, array($this, 'menu'));
		$this->list->Connect(wxEVT_LEFT_DCLICK, array($this, 'open_connection'));
		
		// Add Images to the Image list
		foreach(glob(OSM_RESOURCE_DIR.'/images/*.xpm') as $path)
		{
			//$img = new \wxBitmap(file_get_contents($path));
			//$this->img_list->Add($img);
		}
		
		// Create a button for adding new connections
		$new_conn = new \wxButton($this, self::BTN_ADD, 'New Connection');
		$new_conn->Connect(wxEVT_COMMAND_BUTTON_CLICKED, array($this, 'add_conn'));
		
		// Add the actual connections
		$conns = $this->settings->get_dbs();
		
		foreach($conns as $c)
		{
			$this->list->InsertItem(0, $c->name);
		}
		
		// Add a sizer
		$sizer = new \wxBoxSizer(wxVERTICAL);
		$sizer->add($this->list, 1, wxALL|wxEXPAND);
		$sizer->add($new_conn, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_BOTTOM|wxEXPAND, 5);
		
		$this->SetSizer($sizer);
		$this->Layout();
		$this->Fit();
	}
}

// End of connection_sidebar.php