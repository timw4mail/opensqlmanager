<?php
/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

// --------------------------------------------------------------------------

/**
 * Bootstrap file
 *
 * Initializes parent window and starts the wx event loop
 */

namespace OpenSQLManager;

// --------------------------------------------------------------------------
error_reporting(-1);

// Set the stupid timezone so PHP shuts up.
date_default_timezone_set('GMT');

// Don't set an arbitrary memory limit!
ini_set('memory_limit', -1);

// Set the current directory as the base for included files
define('OSM_BASE_DIR', __DIR__.'/sys');
define('OSM_RESOURCE_DIR', realpath(__DIR__.'/../Resources'));
define('OSM_SETTINGS_DIR', __DIR__);
define('OSM_PROGRAM_NAME', 'OpenSQLManager');
define('OSM_VERSION', '0.2.0pre');

// --------------------------------------------------------------------------

/**
 * Log fatal errors
 */
function log_fatal()
{
	// Catch the last error
	$error = error_get_last();

	// types of errors that are fatal
	$fatal = [E_ERROR, E_PARSE, E_RECOVERABLE_ERROR];

	// Log error.
	if(in_array($error['type'], $fatal))
	{
		file_put_contents('errors.txt', print_r($error, TRUE), FILE_APPEND);
	}
}

register_shutdown_function('OpenSQLManager\log_fatal');

// --------------------------------------------------------------------------

// Make sure wxphp works
if ( ! extension_loaded('wxwidgets'))
{
	// Try to load wxphp if possible
	// Windows is special
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
		$name = 'php_wxwidgets.dll';
	}
	else
	{
		$name = 'wxwidgets.'.PHP_SHLIB_SUFFIX;
	}
	
	$res = dl($name);
		
	if ( ! $res)
	{
		trigger_error("wxPHP not found. Please load the wxPHP extension in your php.ini", E_USER_ERROR);
		die();
	}
}

// Make sure pdo exists
if( ! class_exists('pdo'))
{
	trigger_error("PHP support for PDO is required.", E_USER_ERROR);
	die();
}

// --------------------------------------------------------------------------

/**
 * Error handler to convert errors to exceptions
 *
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @throws ErrorException
 */
function exception_error_handler($errno, $errstr, $errfile, $errline)
{
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
}

// Do this after the two compatibility checks for cleaner output
// Note that this will throw exceptions on notices
set_error_handler('OpenSQLManager\exception_error_handler', -1);

// --------------------------------------------------------------------------

if ( ! function_exists('do_include'))
{
	/**
	 * Bulk directory loading workaround for use
	 * with array_map and glob
	 *
	 * @param string $path
	 * @return void
	 */
	function do_include($path)
	{
		require_once($path);
	}
}

// --------------------------------------------------------------------------

/**
 * Autoloader for OpenSQLManager
 *
 * @param string $class
 */
function osm_autoload($class)
{
	$class_spaces = explode('\\', $class);
	$class = strtolower(end($class_spaces));
	$widget_path = OSM_BASE_DIR . "/widgets/{$class}.php";
	$window_path = OSM_BASE_DIR . "/windows/{$class}.php";

	if (is_file($widget_path))
	{
		require_once($widget_path);
	}
	elseif (is_file($window_path))
	{
		require_once($window_path);
	}
}

// --------------------------------------------------------------------------

// Load all the common classes, and register the autoloader
array_map('OpenSQLManager\do_include', glob(OSM_BASE_DIR.'/common/*.php'));
spl_autoload_register('OpenSQLManager\osm_autoload');

// --------------------------------------------------------------------------

// Auto-load db drivers
require_once(OSM_BASE_DIR . "/db/autoload.php");

// --------------------------------------------------------------------------
// ! App Bootstrap class
// --------------------------------------------------------------------------

/**
 * Class for the app itself
 *
 * @package OpenSQLManager
 */
class OpenSQLManager extends \wxApp {

	/**
	 * Initialize the app
	 *
	 * @return int
	 */
	public function OnInit()
	{
		// The main Window object
		(new Main())->show();

		return 0;
	}

	/**
	 * Return exit code
	 *
	 * @return int
	 */
	public function OnExit()
	{
		\wxExit();
		return 0;
	}
}

// Create the app instance
$app = new OpenSQLManager();

// Define the platform for later use
define('PLATFORM', (new \wxPlatformInfo())->GetOperatingSystemId());

// Start the wx event loop
\wxApp::SetInstance($app);
\wxEntry();

// End of OpenSQLManager.php