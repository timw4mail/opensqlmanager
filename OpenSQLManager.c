/**
 * OpenSQLManager
 *
 * Free Database manager for Open Source Databases
 *
 * @package		OpenSQLManager
 * @author 		Timothy J. Warren
 * @copyright	Copyright (c) 2012
 * @link 		https://github.com/aviat4ion/OpenSQLManager
 * @license 	https://timshomepage.net/dbaj.txt
 */

/**
 * Wrapper program for embeded php
 */

#include <sapi/embed/php_embed.h>

int main(int argc, char *argv[])
{	
	PHP_EMBED_START_BLOCK(argc,argv)
		/* Change extension directory */
		zend_alter_ini_entry("extension_dir", sizeof("extension_dir"), "./", sizeof("./"), PHP_INI_ALL, PHP_INI_STAGE_RUNTIME);
		zend_eval_string(" \
			chdir(dirname($argv[0])); \
			require ('OpenSQLManager.php'); \
		", NULL, "TEST" TSRMLS_CC);
	PHP_EMBED_END_BLOCK()

	return 0;
}
