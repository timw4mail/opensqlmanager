CFLAGS = -c `/opt/php-embed/bin/php-config --includes` -Wall -g
LDFLAGS = -L/Library/Frameworks/Firebird.framework/Libraries -L/opt/php-embed/lib -lphp5 `/opt/php-embed/bin/php-config --libs`

all: OpenSQLManager.c
	${CC} -O0 -o OpenSQLManager.o OpenSQLManager.c ${CFLAGS}
	${CC} -O0 -o OpenSQLManager OpenSQLManager.o ${LDFLAGS}
	
release: OpenSQLManager.c
	${CC} -Os -o OpenSQLManager.o OpenSQLManager.c ${CFLAGS}
	${CC} -Os -o OpenSQLManager OpenSQLManager.o ${LDFLAGS}
	
clean:
	rm -f OpenSQLManager.o
	rm -f OpenSQLManager
	rm -rf OpenSQLManager.app