<?php
// Compile the binary
`make clean`;
`make release`;
`rm OpenSQLManager.o`;

define('EXTENSION_DIR', ini_get('extension_dir'));
define('VERSION', '0.2.0');
define('APP_PATH', __DIR__.'/OpenSQLManager.app');
define('CONTENTS', APP_PATH . '/Contents');
define('FRAMEWORKS', CONTENTS . '/Frameworks');
define('MAC_OS', CONTENTS . '/MacOS');
define('RESOURCES', CONTENTS . '/Resources');
define('SRC_DIR', __DIR__.'/src');
define('SRC_RESOURCES', __DIR__.'/Resources');

if ( ! function_exists('glob_recursive'))
{
	// Does not support flag GLOB_BRACE
	/**
	 * Recursive wrapper for glob
	 */
	function glob_recursive($pattern, $flags = 0)
	{
		$files = glob($pattern, $flags);
	
		foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
		{
			$files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
		}
	
		return $files;
	}
}

/**
 * Creates directories based on the array given
 *
 * @param array $structure
 * @param string $path
 * @return void
 */
function make_dir_tree($structure, $path=__DIR__)
{
	foreach ($structure as $folder => $sub_folder)
	{
		// Folder with subfolders
		if (is_array($sub_folder))
		{
			$new_path = "{$path}/{$folder}";
			if ( ! is_dir($new_path)) mkdir($new_path);
			call_user_func(__FUNCTION__, $sub_folder, $new_path);
		}
		else
		{
			$new_path = "{$path}/{$sub_folder}";
			if ( ! is_dir($new_path)) mkdir($new_path);
		}
	}
}

/**
 * Create bundle folder structure
 */
function create_dirs()
{
	$dir_tree = [
		'OpenSQLManager.app' => [
			'Contents' => [
				'Frameworks',
				'Resources' => [
					'images'
				],
				'MacOS' => [
					'sys' => [
						'common',
						'db' => [
							'classes',
							'drivers' => [
								'mysql',
								'pgsql',
								'sqlite',
								'odbc',
								'firebird'
							]
						],
						'widgets',
						'windows'
					]
				]
			]
		]
	];
	
	make_dir_tree($dir_tree);
}

/**
 * Copy source files and binary into bundle structure
 */
function copy_src()
{
	// Check for binary
	if ( ! file_exists('OpenSQLManager'))
	{
		exit('App bundle not created: binary missing');
	}
	
	// Drop to shell because php treats it as text
	$src = __DIR__.'/OpenSQLManager';
	$dest = MAC_OS;
	`mv {$src} {$dest}`;
	
	// Copy the wxphp extension
	$src = EXTENSION_DIR . '/wxwidgets.so';
	$dest = MAC_OS;
	`cp {$src} {$dest}`;
	
	$raw_src_files = glob_recursive(SRC_DIR.'/*.php');
	$src_files = array();
	
	// Filter test files/directories
	foreach($raw_src_files as $file)
	{
		// Skip test files/directories
		if (stripos($file, 'test') !== FALSE) continue;
		
		$src_files[] = $file;
	}
	
	// Loop through the source files
	foreach($src_files as $file)
	{
		// Figure out the new path
		$new_file = str_replace(SRC_DIR, MAC_OS, $file);
		
		// Copy the files
		copy($file, $new_file);
	}
	
	// Copy license file
	copy(SRC_RESOURCES.'/LICENSE', RESOURCES.'/LICENSE');
	
	// Copy resources
	$rs = glob_recursive(SRC_RESOURCES.'/*.*');
	foreach ($rs as $resource)
	{
		// Figure out new path
		$new_rs = str_replace(SRC_RESOURCES, RESOURCES, $resource);
		
		// Copy the files
		copy($resource, $new_rs);
	}
}

/**
 * Make it an official app
 */
function create_plist()
{
	$version = VERSION;
	
	$plist = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>CFBundleGetInfoString</key>
	<string>App for managing databases.</string>
  <key>CFBundleExecutable</key>
	<string>OpenSQLManager</string>
  <key>CFBundleIdentifier</key>
	<string>com.aviat4ion.OpenSQLManager</string>
  <key>CFBundleName</key>
	<string>OpenSQLManager</string>
  <key>CFBundleIconFile</key>
	<string>OpenSQLManager.icns</string>
  <key>CFBundleShortVersionString</key>
	<string>{$version}</string>
  <key>CFBundleInfoDictionaryVersion</key>
	<string>6.0</string>
  <key>CFBundlePackageType</key>
	<string>APPL</string>
  <key>LSMinimumSystemVersion</key>
	<string>10.6.0</string>
</dict>
</plist>
XML;

	// Add the plist to the bundle
	file_put_contents(CONTENTS.'/Info.plist', $plist);
}

create_dirs();
copy_src();
create_plist();
exit("App bundle created \n");