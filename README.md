#OpenSQLManager

OpenSQLManager is an attempt to create an alternative to Navicat that is free and open. It is build with wxPHP, so I'm looking for a way to create normal binaries. 

[![Build Status](https://secure.travis-ci.org/timw4mail/OpenSQLManager.png)](http://travis-ci.org/timw4mail/OpenSQLManager)

### Pre-configured version of php for windows
I've put together this package from the latest wxPHP windows package. It's available in the downloads section.

### How to run:
* On Windows: drag the `OpenSQLManager.php` file to the `php.exe` or `php-win.exe` executable in the php package. `php.exe` brings up a DOS console, `php-win.exe` does not.
* On Mac: 
	* Install [MacPorts](http://guide.macports.org/#installing)
	* Install these ports using MacPorts
		* php5
		* php5-iconv
		* php5-mysql
		* php5-postgresql
		* php5-sqlite
		* php5-ssh2
		* Firebird support has to be manually compiled
	* Compile wxPHP extension
	* Run via terminal in the OpenSQLManager folder using `php OpenSQLManager.php`

## PHP Requirements
* Version 5.4+
* [wxPHP](http://wxphp.org/) PHP Extension
* OpenSSL
* JSON
* PDO
	* PDO drivers for the databases you wish to use

## Want to Contribute?
See [Dev Guide](https://github.com/aviat4ion/OpenSQLManager/blob/master/DEV_README.md)

## Planned Features
* CRUD (Create, Read, Update, Delete) functionality
* Database table creation and backup 

The databases currently slated to be supported are:

* [Firebird](http://www.firebirdsql.org/) version 2.5+
* [MySQL](http://www.mysql.com/) / [MariaDB](http://mariadb.org/) version 5.1+
* [PostgreSQL](http://www.postgresql.org) version 9.0+
* [SQLite](http://sqlite.org/) version 3+


Plan to implement, not support:

* ODBC

## Won't Support
Closed source DBs, like Oracle, MSSQL, etc. 